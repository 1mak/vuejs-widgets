/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 10/28/19
 * Time: 1:12 PM
 *
 * Installer for CalendarWidget Component
 */

import CalendarWidgetfrom './CalendarWidget'

const install = function (Vue, globalOptions) {
  Vue.component('CalendarWidget', CalendarWidget)
}

export {CalendarWidget, install}