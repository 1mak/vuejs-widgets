/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 12/09/19
 * Time: 1:12 PM
 * 
 * Installer for Loader Component
 */

import Loader from './LoadyMcLoadFace'

const install = function (Vue, globalOptions) {
  Vue.component('Loader', Loader)
}

export {Loader, install}