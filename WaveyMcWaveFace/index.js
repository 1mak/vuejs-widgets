/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 12/09/19
 * Time: 1:12 PM
 * 
 * Installer for WaveyMcWaveFace Component
 */

import WaveyMcWaveFace from './WaveyMcWaveFace'

const install = function (Vue, globalOptions) {
  Vue.component('WaveyMcWaveFace', WaveyMcWaveFace)
}

export {WaveyMcWaveFace, install}