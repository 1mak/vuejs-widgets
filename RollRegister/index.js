/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 10/10/19
 * Time: 1:12 PM
 * 
 * Installer for RollRegister Component
 */

import RollRegister from './RollRegister'

const install = function (Vue, globalOptions) {
  Vue.component('RollRegister', RollRegister)
}

export {RollRegister, install}