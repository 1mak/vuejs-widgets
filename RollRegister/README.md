# RollRegister

This is a styled component that can display card items overlapping each other and animating the z-index on hover (for lack of a better explanation).

Props:

items: Array (list of card items)
limit: Number (maximum of visible items)

This has been used in an ecommerce app hence the RollRegisterItem.vue uses item.priceDiscount to display a price. Change this to whatever you want to display in that content area please.