/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 10/10/19
 * Time: 7:12 PM
 * 
 * Installer for BreadCrumb Component
 */

import BreadCrumbs from './BreadCrumbs'

const install = function (Vue, globalOptions) {
  Vue.component('BreadCrumbs', BreadCrumbs)
}

export {BreadCrumbs, install}