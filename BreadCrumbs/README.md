# BreadCrumbs

This is a simple breadcrumb component which automatically generates and displays a clickable link structure based on the VueRouter object and current route.
The BreadCrumb component does not have )or need) any props at this point.