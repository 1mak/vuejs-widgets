# VueJS Widgets

This repository is a collection of VueJS Single File Components that I've written for multiple applications. I try to keep them here together so I can maintain them easier and do updates more frequently.


**Swoggle**

A light-weight VueJS Toggle/Switch component that can be fully styled and can have a loading state (which uses the Loader.vue from this lib).

![Example of Swoggle Component](https://storage.googleapis.com/openscreenshot/r/u/h/Hygtxghur.png)


**WaveyMcWaveFace**

A SVG wave background component that supports multiple overlapping waves (currently 4) which can also be styled and has a prop for animated waves. Built based on an idea and [code examples of George Tsoukatos](https://codepen.io/okconsumer/pen/gOYEyye)

![Example of Wave Component](https://storage.googleapis.com/openscreenshot/B/u/3/Bk7TVx3uB.png)


**Loader**

A beautiful and light-weight CSS-only loading state animation that can be fully styled and size-adjusted via (S)CSS.

**LikertScale**

A VueJS Component to display a rating scale, often found on survey forms, that measures how people feel about something. It includes a series of questions that you ask people to answer, and displays responses people can choose from.

![Example of Likert Scale Component](https://storage.googleapis.com/openscreenshot/H/_/h/SyNJMeh_H.png)

**BreadCrumbs**

This is a simple VueJS breadcrumb component which automatically generates and displays a clickable link structure based on the VueRouter object and current route.
The BreadCrumb component does not have )or need) any props at this point.

![Example of BreadCrumbs Component](https://storage.googleapis.com/openscreenshot/S/O/3/B1LfGx3OS.png)

**RollRegister**

This is a styled component that can display card items overlapping each other and animating the z-index on hover (for lack of a better explanation).

![Example of RollRegister Component](https://storage.googleapis.com/openscreenshot/r/d/n/SJhAvxndr.png)


**CalenderWidget**

This is a styled component which displays a Calender Date selector on a page. It's prepared to have onclick actions on each date (doSomething()).

![Example of CalendarWidget Component](http://www.nunki.com.au/img/calendarwidget.png)

Demo: https://codepen.io/TorstenMcFly/pen/qBBXYEe
