# LikertScale

A Likert scale is a rating scale, often found on survey forms, that measures how people feel about something. It includes a series of questions that you ask people to answer, and ideally 5-7 balanced responses people can choose from. It often comes with a neutral midpoint.

This component takes the following props:

v-model: Number
question: String (For the label of the Scale)
labels: Array (Includes the label of each radio input)

You need to install `vue-uuid` as dependency.