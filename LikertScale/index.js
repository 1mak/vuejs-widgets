/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 12/09/19
 * Time: 1:12 PM
 * 
 * Installer for LikertScale Component
 */

import LikertScale from './LikertScale'

const install = function (Vue, globalOptions) {
  Vue.component('LikertScale', LikertScale)
}

export {LikertScale, install}