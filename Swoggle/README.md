# Swoggle

This is a styled ToggleSwitch which takes multiple props so it can be branded across different applications and sites.

Props:

value: Boolean
labelOn: String
labelOff: String
color: String (default is '#409eff')
loading: Boolean (shows loading animation which uses the Loader.vue component)
indeterminate: Boolean (shows the toogle handle in the middle to display a non-selected state)