/**
 * Created by PhpStorm.
 * User: Torsten Schmidt (torsten.max.schmidt@gmail.com)
 * Date: 12/09/19
 * Time: 1:12 PM
 * 
 * Installer for Swoggle Component
 */

import Swoggle from './Swoggle'

const install = function (Vue, globalOptions) {
  Vue.component('Swoggle', Swoggle)
}

export {Swoggle, install}